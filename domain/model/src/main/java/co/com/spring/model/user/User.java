package co.com.spring.model.user;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class User {

    private Long id;
    private String name;

}
